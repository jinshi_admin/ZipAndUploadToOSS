﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliyunOssUpload
{

    public interface IExecuteWithoutArguments
    {
        Task ExecuteAsync(Action executeZipAction, Func<Task> executeUploadFunc);
    }
    public class ExecuteWithoutArguments:IExecuteWithoutArguments
    {
        private readonly IOptions<ZipOptions> zipOptions;
        private readonly IOptions<OssOptions> ossOptions;

        public ExecuteWithoutArguments(IOptions<ZipOptions> zipOptions, IOptions<OssOptions> ossOptions)
        {
            this.zipOptions = zipOptions;
            this.ossOptions = ossOptions;
        }
        public async Task ExecuteAsync(Action executeZipAction, Func<Task> executeUploadFunc)
        {
            Console.WriteLine("【Z】 参数执行压缩业务过程");
            Console.WriteLine("【A】 参数 执行上传压缩文件到对象存储");
            Console.WriteLine("【ZA】参数 ZA 系统将先执行压缩后执行上传。");
            Console.WriteLine("请输入参数：");
            string? userInput = null;
            bool zipRequired = false;
            bool upLoadRequired = false;
            foreach (var i in Enumerable.Range(1, 3))
            {
                userInput = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(userInput) || (!userInput.ToUpper().Contains('Z') && !userInput.ToUpper().Contains('A')))
                {
                    Console.WriteLine($"您输入了无效参数,请重试{i}");
                    continue;
                }
                else
                {
                    if (userInput.ToUpper().Contains('Z'))
                    {
                        zipRequired = true;
                    }
                    if (userInput.ToUpper().Contains('A'))
                    {
                        upLoadRequired = true;
                    }
                    break;
                }
            }
            if (!zipRequired && !upLoadRequired)
                return;

            if (zipRequired)
            {
                ShowZipOptions();
            }
            if (upLoadRequired)
            {
                ShowOssOptions();
            }
            void ShowZipOptions()
            {
                Console.WriteLine("压缩配置信息如下：");
                Console.Write(zipOptions.Value.ToString());
                Console.WriteLine();
            }
            void ShowOssOptions()
            {
                Console.WriteLine("上传到对象存储配置信息如下：");
                Console.Write(ossOptions.Value.ToString());
                Console.WriteLine();
            }
            #region 执行压缩文件的业务过程
            if (zipRequired)
            {
                executeZipAction?.Invoke();
                if (!upLoadRequired)
                {
                    Console.WriteLine("按任意键退出...");
                    Console.ReadKey();
                }
            }
            #endregion

            #region 执行上传到对象存储的业务过程
            if (upLoadRequired)
            {
                await executeUploadFunc?.Invoke()!;
                Console.WriteLine("上传成功！按任意键退出");
                Console.ReadKey();
            }
            #endregion
        }
    }
}
